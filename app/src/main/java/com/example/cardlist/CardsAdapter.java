package com.example.cardlist;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.cardlist.databinding.LvCardsRowBinding;

import java.util.List;

public class CardsAdapter extends ArrayAdapter<Card> {


    public CardsAdapter(Context context, int resource, List<Card> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Card card = getItem(position);
        LvCardsRowBinding binding = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_cards_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }

        binding.tvCard.setText(card.getName());
        Glide.with(getContext()).load(
                card.getImageUrl()
        ).apply(new RequestOptions().override(600, 200)).into(binding.ivCardImage);

        return binding.getRoot();
    }

}
