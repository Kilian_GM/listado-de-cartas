package com.example.cardlist.ui.main;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cardlist.Card;
import com.example.cardlist.SharedViewModel;
import com.example.cardlist.databinding.FragmentDetailBinding;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;

    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater);

        view = binding.getRoot();
        Intent i = getActivity().getIntent();

        if (i != null) {
            Card card = (Card) i.getSerializableExtra("card");
            if (card != null) {
                updateUI(card);
            }
        }

        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Card>() {
            @Override
            public void onChanged(@Nullable Card card) {
                updateUI(card);
            }
        });

        return view;
    }

    private void updateUI(Card card) {
        Log.d("Card", "XXXXXXXXXXXXXXXXXXXXXXXXXXX"+card.toString());

        binding.tvType.setText(card.getType());
        binding.tvRarity.setText(card.getRarity());
        binding.tvName.setText(card.getName());

        Glide.with(getContext()).load(
                card.getImageUrl()
        ).into(binding.ivCardImage);
    }

}
