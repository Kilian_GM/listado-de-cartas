package com.example.cardlist.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.cardlist.CardViewModel;
import com.example.cardlist.Card;
import com.example.cardlist.CardDBAPI;
import com.example.cardlist.CardsAdapter;
import com.example.cardlist.ui.main.DetailActivityFragment;
import com.example.cardlist.R;
import com.example.cardlist.SharedViewModel;
import com.example.cardlist.databinding.MainFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment {

    private CardViewModel mViewModel;
    private ArrayList<Card> items;
    private CardsAdapter adapter;
    private ProgressDialog dialog;
    private MainFragmentBinding binding;
    private SharedViewModel sharedModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();
        items = new ArrayList<>();

        adapter = new CardsAdapter(
                getContext(),
                R.layout.lv_cards_row,
                items
        );
        sharedModel = ViewModelProviders.of(getActivity()).get(
                SharedViewModel.class
        );

        binding.lvCards.setAdapter(adapter);
        binding.lvCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Card card = (Card) adapterView.getItemAtPosition(i);
                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("Cards", card);
                    startActivity(intent);
                } else {
                    sharedModel.select(card);
                }
            }
        });


        mViewModel.getCards().observe(this, new Observer<List<Card>>() {
            @Override
            public void onChanged(@Nullable List<Card> cards) {
                adapter.clear();
                adapter.addAll(cards);
            }
        });
        return view;
    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_refresh){
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cards_fragment, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");
        mViewModel = ViewModelProviders.of(this).get(CardViewModel.class);
        mViewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if (mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });
        refresh();
    }

    private void refresh() {
        mViewModel.reload();
    }


}
