package com.example.cardlist;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CardDBAPI {
    private final String BASE_URL = "https://api.magicthegathering.io";

    public ArrayList<Card> getCards() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }


    private ArrayList<Card> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Card> processJson(String jsonResponse) {
        ArrayList<Card> cards = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCards = data.getJSONArray("cards");
            for (int i = 0; i < jsonCards.length(); i++) {
                JSONObject jsonMovie = jsonCards.getJSONObject(i);

                Card card = new Card();
                card.setName(jsonMovie.getString("name"));
                card.setRarity(jsonMovie.getString("rarity"));
                card.setType(jsonMovie.getString("type"));
                if (jsonMovie.has("imageUrl"))
                    card.setImageUrl(jsonMovie.getString("imageUrl"));


                cards.add(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cards;
        }
    }

