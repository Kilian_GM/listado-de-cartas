package com.example.cardlist;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Card> selected = new MutableLiveData<Card>();

    public void select(Card Card) {
        selected.setValue(Card);
    }

    public LiveData<Card> getSelected() {
        return selected;
    }
}
