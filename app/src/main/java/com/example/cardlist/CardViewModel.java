package com.example.cardlist;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cardlist.AppDatabase;
import com.example.cardlist.Card;
import com.example.cardlist.CardDBAPI;

import java.util.ArrayList;
import java.util.List;

public class  CardViewModel extends AndroidViewModel {
    public final Application app;
    public final AppDatabase appDatabase;
    public final com.example.cardlist.CardDao CardDao;
    public MutableLiveData<Boolean> loading;
    public LiveData<List<Card>> cards;


    public void setCards(LiveData<List<Card>> cards) {
        this.cards = cards;
    }

    public CardViewModel(Application application) {
        super(application);
        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.CardDao = appDatabase.getCardDao();
    }

    public LiveData<List<Card>> getCards() {
        return CardDao.getCards();
    }

    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            CardDBAPI api = new CardDBAPI();
            ArrayList<Card> result;
            result = api.getCards();
            CardDao.deleteCards();
            CardDao.addCards(result);
            return result;
        }
        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            super.onPostExecute(cards);
            loading.setValue(false);
        }
    }

}

