package com.example.cardlist;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import java.util.List;

@Dao
public interface CardDao {
    @Query("SELECT * FROM Card")
    LiveData<List<Card>> getCards();

    @Insert
    void addCard(Card card);

    @Insert
        void addCards(List<Card> cards);

    @Delete
    void deleteCard(Card card);

    @Query("DELETE FROM Card")
    void deleteCards();



}
