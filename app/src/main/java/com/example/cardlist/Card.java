package com.example.cardlist;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Arrays;

@Entity
public class Card implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int cmc;
    private String type;
    private String manaCost;
    private String rarity;
    private String imageUrl;

    public Card() {
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setId(int id) {
        this.id = id;
    }

    /*public Card(int id, String[] colorIdentity, String name, String[] colors, int cmc, String type, String manaCost, String rarity, String imageUrl) {
        this.id = id;
        this.colorIdentity = colorIdentity;
        this.name = name;
        this.colors = colors;
        this.cmc = cmc;
        this.type = type;
        this.manaCost = manaCost;
        this.rarity = rarity;
        this.imageUrl = imageUrl;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCmc() {
        return cmc;
    }

    public void setCmc(int cmc) {
        this.cmc = cmc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getManaCost() {
        return manaCost;
    }

    public void setManaCost(String manaCost) {
        this.manaCost = manaCost;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    @Override
    public String toString() {
        return "Card{" +
                ", name='" + name + '\'' +
                ", cmc=" + cmc +
                ", type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", manaCost='" + manaCost + '\'' +
                ", rarity='" + rarity + '\'' +
                '}';
    }
}

